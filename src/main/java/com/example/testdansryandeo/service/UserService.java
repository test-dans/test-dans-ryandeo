package com.example.testdansryandeo.service;

import com.example.testdansryandeo.model.entity.UserEntity;
import com.example.testdansryandeo.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    IUserRepository iUserRepository;

    public UserEntity findByUsername(String username){
        return iUserRepository.findByUsername(username);
    }
}
