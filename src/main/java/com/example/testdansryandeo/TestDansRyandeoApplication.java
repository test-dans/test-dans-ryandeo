package com.example.testdansryandeo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDansRyandeoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDansRyandeoApplication.class, args);
	}

}
