package com.example.testdansryandeo.controller;

import com.example.testdansryandeo.model.entity.UserEntity;
import com.example.testdansryandeo.model.req.ReqUser;
import com.example.testdansryandeo.model.res.StdResponse;
import com.example.testdansryandeo.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("v1.dansmultipro/api")
public class UserController {
    @Value("${uri.job.list}")
    private String uriJobList;

    @Value("${uri.job.detail}")
    private String uriJobDetail;

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public ResponseEntity<StdResponse<UserEntity>> login(@RequestBody ReqUser reqUser){
        String username = reqUser.getUsername();
        String password = reqUser.getPassword();
        UserEntity ue = userService.findByUsername(username);
        StdResponse<UserEntity> response=new StdResponse<>(HttpStatus.BAD_REQUEST.toString(),"",null);
        if(ue == null){
            return ResponseEntity.badRequest().body(response);
        }
        if (password.equals(ue.getPassword())) {
            response = new StdResponse<>(HttpStatus.OK.toString(), "", ue);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<StdResponse<UserEntity>>(response, HttpStatus.OK);
    }

    @GetMapping("/job")
    public ResponseEntity<List<HashMap<String,Object>>> getListJob(){
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uriJobList, String.class);
        JSONArray jsonArray = new JSONArray(result);
        Type type = new TypeToken<List<HashMap<String,Object>>>(){}.getType();
        List<HashMap<String,Object>> hashMapList = new Gson().fromJson(jsonArray.toString(), type);

        return new ResponseEntity<List<HashMap<String,Object>>>(hashMapList, HttpStatus.OK);
    }

    @GetMapping("/job/{id}")
    public ResponseEntity<HashMap<String,Object>> getDetailJob(@PathVariable String id){
        String uri = uriJobDetail + "/" + id;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        JSONObject jsonObject = new JSONObject(result);

        HashMap<String,Object> mapRes = new Gson().fromJson(jsonObject.toString(), HashMap.class);

        return new ResponseEntity<HashMap<String,Object>>(mapRes, HttpStatus.OK);
    }

    @GetMapping("/createCSV")
    public void createCSV(HttpServletResponse response) throws IOException{
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uriJobList, String.class);
        JSONArray jsonArray = new JSONArray(result);
        Type type = new TypeToken<List<HashMap<String,Object>>>(){}.getType();
        List<HashMap<String,Object>> hashMapList = new Gson().fromJson(jsonArray.toString(), type);

        response.setContentType("text/csv");
        response.addHeader("Content-Disposition", "attachment; filename=\"job.csv\"");
        writeStudentsToCsv(hashMapList, response.getWriter());
    }

    public void writeStudentsToCsv(List<HashMap<String,Object>> jobs, Writer writer) {
        try {

            CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
            for (HashMap<String,Object> job : jobs) {
                printer.printRecord(job.get("company_logo"), job.get("how_to_apply"), job.get("created_at"),
                        job.get("description"), job.get("company"), job.get("company_url"),
                        job.get("location"), job.get("id"), job.get("type"), job.get("title"), job.get("url"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
