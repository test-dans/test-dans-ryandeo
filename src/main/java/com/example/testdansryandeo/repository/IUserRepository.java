package com.example.testdansryandeo.repository;

import com.example.testdansryandeo.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IUserRepository extends JpaRepository<UserEntity, Integer> {
    @Query(value = "select ue from UserEntity ue where ue.username = :username")
    public UserEntity findByUsername(@Param("username") String username);
}
